package id.ac.ub.papb.recycler1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.MahasiswaViewHolder> {
    LayoutInflater inflater;
    Context _context;
    ArrayList<Mahasiswa> data;


//    constructor
    public MahasiswaAdapter(Context _context, ArrayList<Mahasiswa> data) {
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
    }

    @NonNull
    @Override
//    Layout Manager
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
//    set satu tampilan list dengan nama dan nim pada index position
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {
        Mahasiswa mhs = data.get(position);
        Log.d(MainActivity.TAG,"data "+position);
        Log.d(MainActivity.TAG,"nim "+mhs.nim);
        Log.d(MainActivity.TAG,"nama "+mhs.nama);

        holder.tvNim.setText(mhs.nim);
        holder.tvNama.setText(mhs.nama);

        holder._layRow.setOnClickListener(view -> {
            Intent intent = new Intent(_context, Activity2.class);
            Bundle bundle = new Bundle();
            bundle.putString("NIM", holder.tvNim.getText().toString());
            bundle.putString("NAMA", holder.tvNama.getText().toString());
            intent.putExtras(bundle);
            _context.startActivity(intent);
        });

    }

    @Override
//    jumlah isi data dari objek mahasiswa (nim dan nama) berupa array list
    public int getItemCount() {
        Log.d(MainActivity.TAG,"Jumlah data "+data.size());
        return data.size();
    }

    //    Class ViewHolder
    class MahasiswaViewHolder extends RecyclerView.ViewHolder {
        TextView tvNim;
        TextView tvNama;
        ConstraintLayout _layRow;
//        Context context;

        public MahasiswaViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNama = itemView.findViewById(R.id.tvNama);
            _layRow = itemView.findViewById(R.id.layRow);
        }

        public TextView getTvNim() {
            return tvNim;
        }

        public TextView getTvNama() {
            return tvNama;
        }

    }
}